  <%--
    Created by IntelliJ IDEA.
    User: pkrishnan
    Date: 10/19/2016
    Time: 3:49 PM
    To change this template use File | Settings | File Templates.
  --%>
  <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
           pageEncoding="ISO-8859-1"%>
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>

  <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

  <h1>User Data</h1>

  <form:form method="GET"  commandname="user-entity"  modelAttribute="userData"   id="userData" >
    <table>

      <tr>
        <td>First Name</td>
        <td><input id=firstName type="text" ></td>
      </tr>
      <tr>
        <td>Last Name</td>
        <td><input  id=lastName type="text">
        </td></tr>
      <tr>
        <td colspan="2"><input id="save" value="Save Record" type="submit"
                               class="blue-button" /></td>
      </tr>
    </table>
  </form:form>


  <div id="addResponse">
  </div>


  <div>
   <input id="submit" type="submit" value="Display Records"></input>
  </div>


  <div id="userquerytable-container"></div>




  <script>
    jQuery(document).ready(function($) {

      $('#submit').click(function(e){
        //$('#submit').attr("disabled", 'disabled');
        searchViaAjax();
        e.preventDefault();

      });

    });

    jQuery(document).ready(function($) {

        $('#save').click(function(e){
        //  $('#save').attr("disabled", 'disabled');
        addData();
          e.preventDefault();
      });
    });


    function searchViaAjax() {


      var data = {}

      $.ajax({
        type : "GET",
        contentType : "application/json",
        data : JSON.stringify(),
        url : "/rest/User/ReadUserDetails",
        dataType : 'json',
        timeout : 100000,
        success : function(data) {
          console.log("SUCCESS: ", data);
          display(data);
        },
        error : function(e) {
          console.log("ERROR: ", e);
          display(e);
        },
        done : function(e) {
          console.log("DONE");
          enableSearchButton(true);
        }
      });


    }


    function addData() {

     $.ajax({
        type : "GET",
        contentType : "application/json",
        data : {


          "fName": $("#firstName").val(),
          "lName": $("#lastName").val()

        },
        url : "/rest/User/AddUserDetails",
        dataType : 'json',
        timeout : 100000,
        success : function(data) {
          console.log("SUCCESS: ", data);
          displayAdd("SUCCESS");
        },
        error : function(e) {
          console.log("ERROR: ", e);
          displayAdd("ERROR");
        },
        done : function(e) {
          console.log("DONE");
          //enableSearchButton(true);
        }
      });


    }



    function display(data) {

        $( "#userquerytable-container" ).html ("");
        var colData = ["First Name", "Last Name"];
        var rowData = JSON.parse(JSON.stringify(data));
        var data = {"Cols":colData, "Rows":rowData};

        var table = $('<table/>').attr("id", "userquerytable").addClass("display").attr("cellspacing", "0").attr("width", "100%").attr("border","1px solid black");

        var tr = $('<tr/>');
        table.append('<thead>').children('thead').append(tr);

        for (var i=0; i< data.Cols.length; i++) {
            tr.append('<td>'+data.Cols[i]+'</td>');
        }

        for(var r=0; r < data.Rows.length; r++){
            var tr = $('<tr/>');
            table.append(tr);
            //loop through cols for each row...

                tr.append('<td>'+data.Rows[r].firstName+'</td>');
            tr.append('<td>'+data.Rows[r].lastName+'</td>');
        }

        $('#userquerytable-container').append(table);



    }



    function displayAdd(data) {
        $( "#addResponse" ).html ("");
        $('#addResponse').append(data);

     }





  </script>