package com.planplus.controller;


import com.planplus.model.UserData;
import com.planplus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;

/**
 * Created by pkrishnan on 10/19/2016.
 */
@Controller
@Configuration
@ComponentScan("com.planplus")
@RequestMapping("/User")
public class UserController {


    @Autowired
    private UserService _userService;

    @RequestMapping("/HomePage")
    public @ResponseBody ModelAndView displayPage ()  {
         return new ModelAndView("index", "user-entity", new UserData());
    }

        @RequestMapping("/AddUserDetails")
        public @ResponseBody Boolean addUserDetails(  @RequestParam (defaultValue = "") String fName ,@RequestParam (defaultValue = "")String lName ) {


            if(fName !="" ||lName!="" ){
            UserData userData1 = new UserData();
            userData1.setFirstName(fName);
            userData1.setLastName(lName);
             Boolean retValue =  _userService.addUserDeatils(userData1);
            if (retValue) {

                return true;
            }
            }
            return false;
    }

    @RequestMapping("/ReadUserDetails")
    public @ResponseBody
    ArrayList <UserData>  getUserDetails() {
        return _userService.getUserDeatils();

    }

}
