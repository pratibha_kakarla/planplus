-- Table creation Script

CREATE TABLE UserData
(
UserID INT  NOT NULL AUTO_INCREMENT, 
FirstName VARCHAR(255),
LastName VARCHAR(255),
PRIMARY KEY (UserID),
CONSTRAINT UK_Name UNIQUE (FirstName,LastName)
);