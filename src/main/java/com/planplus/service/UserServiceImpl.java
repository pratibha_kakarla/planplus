package com.planplus.service;

import com.planplus.model.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by pkrishnan on 10/19/2016.
 */

@Service
public class UserServiceImpl implements UserService {

@Autowired
    private DataSource dataSource;

    public Boolean addUserDeatils(UserData userData){

        String sql = "INSERT INTO UserData " +
                "( FirstName, LastName) VALUES ( ?, ?)";
        Connection conn = null;

        try {

            conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, userData.getFirstName());
            ps.setString(2, userData.getLastName());
            ps.executeUpdate();
            ps.close();
            return true;
        } catch (SQLException e) {
            throw new RuntimeException(e);

        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }

    }

    public ArrayList <UserData> getUserDeatils() {
        String sql = "SELECT * FROM UserData";

        Connection conn = null;

        try {
              conn = dataSource.getConnection();
            PreparedStatement ps = conn.prepareStatement(sql);

            ArrayList <UserData> userDataList  = new ArrayList<UserData>();

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                UserData userData = new UserData();
                userData.setFirstName(rs.getString("FirstName"));
                userData.setLastName(rs.getString("LastName"));
                userDataList.add(userData);
            }
            rs.close();
            ps.close();
            return userDataList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {}
            }
        }
    }

}
