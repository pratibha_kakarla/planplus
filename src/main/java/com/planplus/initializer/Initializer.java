package com.planplus.initializer;


import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;


/**
 * Created by pkrishnan on 10/19/2016.
 */

    public class Initializer implements WebApplicationInitializer {

        public void onStartup(ServletContext servletContext) throws ServletException {


            AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
            ctx.register(WebAppConfig.class);
            servletContext.addListener(new ContextLoaderListener(ctx));

            ctx.setServletContext(servletContext);

            Dynamic servlet = servletContext.addServlet("dispatcher",
                    new DispatcherServlet(ctx));
            servlet.setInitParameter("contextConfigLocation", "/WEB-INF/mvc-dispatcher-servlet.xml");
            servlet.addMapping("/rest/*");

            servlet.setLoadOnStartup(1);


        }
    }

